//
//  PromoterClub2Tests.m
//  PromoterClub2Tests
//
//  Created by ILYA LEVIN on 5/11/13.
//  Copyright (c) 2013 ILYA LEVIN. All rights reserved.
//

#import "PromoterClub2Tests.h"
#import "VIPRequestService.h"

@implementation PromoterClub2Tests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
    service = [[VIPRequestService alloc] init];
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testSave
{
    VIPRequest *obj1 = [[VIPRequest alloc] initwithName:@"Hello" AndGuestNum:5 AndWithTable:YES AndWithContactPrefs:email AndWithContactValue:@"hello@gmail.com" AndWithPromoContact:@"test@test.com"];
    [service clearHistory];
    [service pushToRequestHistory:obj1];
    [service save];
    service = [[VIPRequestService alloc ]init];
    NSString *name = [(VIPRequest *)[service.requestHistory objectAtIndex:0] name];
    STAssertTrue([name isEqual:@"Hello"],@"If this fails something is wrong with the history");
}

@end

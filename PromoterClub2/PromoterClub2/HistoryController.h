//
//  HistoryController.h
//  PromoterClub2
//
//  Created by ILYA LEVIN on 5/15/13.
//  Copyright (c) 2013 ILYA LEVIN. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VIPRequestService.h"

@interface HistoryController : UITableViewController

@property(strong,nonatomic) VIPRequestService *service;
- (IBAction)refresh:(id)sender;

@end

//
//  RequestCell.h
//  PromoterClub2
//
//  Created by ILYA LEVIN on 5/15/13.
//  Copyright (c) 2013 ILYA LEVIN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RequestCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *NameText;
@property (weak, nonatomic) IBOutlet UILabel *GuestText;
@property (weak, nonatomic) IBOutlet UILabel *TableText;
@property (weak, nonatomic) IBOutlet UILabel *StatusText;


@end

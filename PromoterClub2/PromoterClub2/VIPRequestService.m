//
//  VIPRequestService.m
//  PromoterClub2
//
//  Created by ILYA LEVIN on 5/12/13.
//  Copyright (c) 2013 ILYA LEVIN. All rights reserved.
//

#import "VIPRequestService.h"
#import "VIPRequest.h"
@implementation VIPRequestService

@synthesize delegate;
@synthesize requestHistory;

NSString *baseURL = @"http://webbiebuilder.com/e75/final.php?method=vipRequest&name=%@&guest=%d&table=%@&contactPrefs=%@&contact=%@&promoContact=%@";

NSString * const contactMethodDisplay[] = {
    @"e-Mail",
    @"SMS"
};

int const stackLimit = 10;


-(id)init{
    self = [super init];
    if(self){
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"config" ofType:@"plist"];
        NSDictionary *config = [NSDictionary dictionaryWithContentsOfFile:plistPath];
        lastpositionHistory = [[config objectForKey:@"lastposition"] intValue];
        
        [self loadHistory];
    }
    return self;
}


-(void)sendRequest:(VIPRequest *)request{
    [self pushToRequestHistory:request];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    NSString *urlString = [NSString stringWithFormat:baseURL,request.name,request.guestNum,request.withTable?@"YES":@"NO",contactMethodDisplay[request.contactPrefs],request.contactValue,request.promoContact];
    
    NSURL *httpLink = [NSURL URLWithString:urlString];
    NSLog(@"%@",urlString);
    
    dispatch_async(queue, ^{
        NSData* data = [NSData dataWithContentsOfURL:
                        httpLink];
        [self performSelectorOnMainThread:@selector(fetchedData:)
                               withObject:data waitUntilDone:YES];
    });
    [self.delegate sendingRequest];
}

- (void)fetchedData:(NSData *)responseData {
    //parse out the json data
    NSError* error = [[NSError alloc] init];
    if(responseData){
        NSDictionary* results = [NSJSONSerialization
                              JSONObjectWithData:responseData
                              options:kNilOptions
                              error:&error];
        if([[results objectForKey:@"results"] isEqual:@"Success"]){
            [self.delegate requestSucessful:results];
        }else{
            [self.delegate requestFailed:results];
        }
    }else{
        [self.delegate noConnection];
    }
}

-(BOOL)save{
    BOOL results = [NSKeyedArchiver archiveRootObject:self.requestHistory toFile:[self getHistoryPath]];
    return results;
}

-(void)clearHistory{
    self.requestHistory = [[NSMutableArray alloc] init];
    [self save];
}

-(void)loadHistory{
    self.requestHistory = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithFile:[self getHistoryPath]];
    if (self.requestHistory==nil) {
        self.requestHistory = [[NSMutableArray alloc] init];
    }
    
}

-(void)pushToRequestHistory:(VIPRequest *)request{
    lastpositionHistory++;
    lastpositionHistory = lastpositionHistory % stackLimit;
    [requestHistory addObject:request];
}

-(NSString *)getHistoryPath{
    NSArray *document = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [document objectAtIndex:0];
    path = [path stringByAppendingString:@"/requests4.data"];
    return path;
}


@end

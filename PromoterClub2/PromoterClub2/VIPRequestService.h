//
//  VIPRequestService.h
//  PromoterClub2
//
//  Created by ILYA LEVIN on 5/12/13.
//  Copyright (c) 2013 ILYA LEVIN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VIPRequest.h"

@protocol RequestEvents <NSObject>

-(void)sendingRequest;
-(void)noConnection;
-(void)requestSucessful:(NSDictionary*)response;
-(void)requestFailed:(NSDictionary*)response;

@end


@interface VIPRequestService : VIPRequest{
    int lastpositionHistory;
}



-(void)sendRequest:(VIPRequest *)request;
-(BOOL)save;
-(void)clearHistory;
-(void)pushToRequestHistory:(VIPRequest *)request;
-(id)init;

@property(nonatomic,retain) NSMutableArray *requestHistory;
@property(nonatomic,retain) id<RequestEvents> delegate;

@end

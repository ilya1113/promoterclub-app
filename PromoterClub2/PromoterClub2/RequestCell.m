//
//  RequestCell.m
//  PromoterClub2
//
//  Created by ILYA LEVIN on 5/15/13.
//  Copyright (c) 2013 ILYA LEVIN. All rights reserved.
//

#import "RequestCell.h"

@implementation RequestCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
